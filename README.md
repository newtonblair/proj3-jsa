#Student Author and Contact Information
Newton Blair
nblair@uoregon.edu

##Software Description
This software is a server for a Anagram mini game that using a set list of words creates a semi-random jumble of letters in which the users then attempts to make three words from the list with. The user will type in
the word in the text box, and upon successful typing of a new word the page automatically updates posting the new found words to your found word list. Once you have hit the target amount of words the server
sends you to the success page to indicate the successful completion of the game.

##Build and run container
Build the simple flask app image using

docker build -t newton-proj3 .

Run the container using
docker run -d -p 5000:5000 newton-proj3

Launch the game webpage using web browser url http://127.0.0.1:5000

# proj3-JSA
Vocabulary anagrams game for primary school English language learners (ELL)

## Overview

A simple anagram game designed for English-language learning students in elementary and middle school. Students are presented with a list of vocabulary words (taken from a text file) and an anagram. The anagram is a jumble of some number of vocabulary words, randomly chosen. Students attempt to type words that can be created from the jumble. When a matching word is typed, it is added to a list of solved words. 

The vocabulary word list is fixed for one invocation of the server, so multiple students connected to the same server will see the same vocabulary list but may  have different anagrams.

## Authors 

Initial version by M Young; Docker version added by R Durairajan; to be revised by CIS 322 students. 
